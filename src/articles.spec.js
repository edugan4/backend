/*
 * Test suite for articles.js
 */
const expect = require('chai').expect;
const fetch = require('isomorphic-fetch');

const url = path => `http://localhost:3000${path}`;

describe('Validate Article functionality', () => {

	// adding an article,
	// i.e., GET and count the current number of articles,
	// POST a new article, verify you get back what you added,
	// then GET again and count the number verifying it increased by one
	it('should correctly add an article', (done) => {
		fetch(url('/login'), {
			method: 'post',
			credentials: 'include',
			body: {
				// use my existing test user
				username: 'test0',
				password: 'n0t3st',
				headers: new Headers({
          'content-type': 'application/json; charset=utf-8',
          'Accept': 'application/json'
				})
			}
		})
		.then(res => {
			fetch(url("/articles"), { credentials: 'include' })
			.then(res => {
				expect(res.status).to.eql(200);
				return res.text();
			})
			.then(text => {
				let jsonText = JSON.parse(text);
				expect(jsonText.length).to.be(10);
			})
		})
		.then(res => {
			fetch(url('/article'), {
				method: 'post',
				credentials: 'include',
				body: {
					text: 'a new test article!!!!',
					headers: new Headers({
						'content-type': 'application/json; charset=utf-8',
						'Accept': 'application/json'
					})
				}
			})
			.then(res => {
				expect(res.status).to.eql(200);
				return res.text();
			})
			.then(text => {
				let jsonText = JSON.parse(text);
				expect(jsonText.articles[0].text).to.eql('a new test article!!!!');
			})
		})
		.then(res => {
			fetch(url('/articles'), { credentials: 'include' })
			.then(res => {
				expect(res.status).to.eql(200);
				return res.text();
			})
			.then(text => {
				let jsonText = JSON.parse(text);
				expect(jsonText.length).to.be(11);
			})
		})
		.then(done)
		.catch(done)
 	}, 2000)
});
