// this is model.js
var mongoose = require('mongoose');
require('./db.js');

var userSchema = new mongoose.Schema({
	username: String,
  salt: String,
  hash: String
});

var profilesSchema = new mongoose.Schema({
	username: String,
  status: String,
  following: [ String ],
  email: String,
  zipcode: String,
  picture: String,
	dob: String
});

var commentSchema = new mongoose.Schema({
	commentId: Number,
	author: String,
	date: Date,
	text: String
});

var articleSchema = new mongoose.Schema({
	id: Number,
	author: String,
	img: String,
	date: Date,
	text: String,
	comments: [ commentSchema ]
});

exports.Article = mongoose.model('articles', articleSchema);
exports.User = mongoose.model('users', userSchema);
exports.Profile = mongoose.model('profiles', profilesSchema);
