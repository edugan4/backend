const loginChecks = require('./loginChecks.js');
var models = require('./model.js');
const uploadImage = require('../uploadCloudinary');

const getHeadlines = (req, res) => {
  // if no users in params, use loggedInUser
  const users = req.params.users ? req.params.users.split(',') : [ req.username ];
  models.Profile.find({
    username: {
      $in: users
    }
  }, function(err, profiles) {
    if (err) {
      console.log("Error finding profiles in headlines");
      return res.sendStatus(500);
    }
    if (profiles.length === 0) {
      console.log("No matching profiles found.");
      return res.sendStatus(500);
    }
    let headlines = profiles.map(function(profile) {
      return {
        username: profile.username,
        headline: profile.status
      }
    });
    return res.send({ headlines: headlines });
  });
}

const updateHeadline = (req, res) => {
  if (!req.username) {
    return res.status(401);
  }
  if (!req.body.headline) {
    return res.sendStatus(400);
  }
  let callback = (err, items) => {
    if (err) {
      return res.send(500, { error: err });
    }
    return res.send({ username: req.username, headline: req.body.headline });
  };
  models.Profile.findOneAndUpdate(
    { username: req.username },  // query
    { status: req.body.headline},  // newData
    { upsert: false },  // do not create if not found
    callback);
}

const getEmail = (req, res) => {
  if (!req.username) {
    return res.status(401);
  }
  const user = req.params.user ? req.params.user : req.username;

  models.Profile.findOne({ username: user }, function(err, profile) {
    if (err) {
      console.log("getEmail: error finding profile.");
      return res.sendStatus(500);
    }
    res.send({ username: profile.username, email: profile.email });
  });
}

const updateEmail = (req, res) => {
  if (!req.username) {
    res.sendStatus(401);
  }
  if (!req.body.email) {
    res.sendStatus(400);  // invalid request
  }
  let callback = function(err, profile) {
    if (err) {
      return res.send(500, { error: err });
    }
    return res.send({ username: req.username, email: req.body.email });
  };
  models.Profile.findOneAndUpdate(
    { username: req.username },  // query
    { email: req.body.email },  // newData
    { upsert: false },  // do not create if not found
    callback);
}

const getZIP = (req, res) => {
  if (!req.username) {
    return res.status(401);
  }
  const user = req.params.user ? req.params.user : req.username;

  models.Profile.findOne({ username: user }, function(err, profile) {
    if (err) {
      console.log("getZip: error finding profile.");
      return res.sendStatus(500);
    }
    res.send({ username: profile.username, zipcode: profile.zipcode });
  });
}

const updateZIP = (req, res) => {
  if (!req.username) {
    res.sendStatus(401);
  }
  if (!req.body.zipcode) {
    res.sendStatus(400);  // invalid request
  }
  let callback = function(err, profile) {
    if (err) {
      return res.send(500, { error: err });
    }
    return res.send({ username: req.username, zipcode: req.body.zipcode });
  };
  models.Profile.findOneAndUpdate(
    { username: req.username },  // query
    { zipcode: req.body.zipcode },  // newData
    { upsert: false },  // do not create if not found
    callback);
}

const getAvatar = (req, res) => {
  // if no users in params, use loggedInUser
  const users = req.params.users ? req.params.users.split(',') : [ req.username ];
  models.Profile.find({
    username: {
      $in: users
    }
  }, function(err, profiles) {
    if (err) {
      console.log("Error finding profiles in getAvatar");
      return res.sendStatus(500);
    }
    if (profiles.length === 0) {
      console.log("getAvatar: no matching profiles found.");
      return res.sendStatus(500);
    }
    let avatars = profiles.map(function(profile) {
      return {
        username: profile.username,
        avatar: profile.picture
      }
    });
    return res.send({ avatars: avatars });
  });
}

const updateAvatar = (req, res) => {
  if (!req.username) {
    res.sendStatus(401);
  }
  if (!req.fileurl) {
    return res.sendStatus(400);  // invalid request
  }
  let callback = function(err, profile) {
    if (err) {
      return res.send(500, { error: err });
    }
    return res.send({ username: req.username, avatar: req.fileurl });
  };
  models.Profile.findOneAndUpdate(
    { username: req.username },  // query
    { picture: req.fileurl },  // newData
    { upsert: false },  // do not create if not found
    callback);
}

const getDob = (req, res) => {
  if (!req.username) {
    return res.status(401);
  }
  models.Profile.findOne({ username: req.username }, function(err, profile) {
    if (err) {
      console.log("getDob: error finding profile.");
      return res.sendStatus(500);
    }
    res.send({ username: req.username, dob: profile.dob });
  });
}

module.exports = (app) => {
  app.get('/headlines/:users*?', loginChecks.isLoggedIn, getHeadlines);
  app.put('/headline', loginChecks.isLoggedIn, updateHeadline);
  app.get('/email/:user?', loginChecks.isLoggedIn, getEmail);
  app.put('/email', loginChecks.isLoggedIn, updateEmail);
  app.get('/zipcode/:user*?', loginChecks.isLoggedIn, getZIP);
  app.put('/zipcode', loginChecks.isLoggedIn, updateZIP);
  app.get('/avatars/:users*?', loginChecks.isLoggedIn, getAvatar);
  app.put('/avatar', loginChecks.isLoggedIn, uploadImage('avatar'), updateAvatar);
  app.get('/dob', loginChecks.isLoggedIn, getDob);
}
