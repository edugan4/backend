const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const loginChecks = require('./loginChecks.js');
var session = require('express-session');
var models = require('./model.js');
const md5 = require('md5');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var mongoUrl = "mongodb://testuser:n0t3st@ds149998.mlab.com:49998/ricebook-ejd6";

// login session stuff
var cookieKey = 'sid';
var mySecretMessage = "Your friends... What kind of... people are they? I wonder... Do these people... think of you... as a friend?";
// /middleware

function getUser(username, callback) {
  models.User.find({ username: username }).exec(function(err, items) {
    callback(items);
  });
}

function addUser(
  username,
  password,
  email,
  zipcode,
  dob
) {
  let pepper = md5(mySecretMessage);
  let salt = md5(username + new Date().getTime());
  let hash = md5(salt + password + pepper);
  let newUser = {
    username: username,
    salt: salt,
    hash: hash
  };
  // TODO: check if username already exists...
  models.User.create(newUser, function(err, user) {
    if (err) {
      console.log("Error: error creating new user");
    }
  });

  let newProfile = {
    username: username,
    status: 'default status',
    following: [ ],
    email: email,
    zipcode: zipcode,
    picture: '',
  	dob: dob
  }
  models.Profile.create(newProfile, function(err, profile) {
    if (err) {
      console.log("Error: error creating new profile");
    }
  });
}

// generates a session code for a logged in user.
function generateCode(userObj) {
  return md5(md5(mySecretMessage) + new Date().getTime() + userObj.username);
}

const login = (req, res) => {
  if (req.username) {
    console.log("Error: already logged in.");
    return;
  }
  let username = req.body.username;
  let password = req.body.password;
  if (!username || !password) {
    return res.sendStatus(400);
  }
  getUser(username, (items) => {
    if (items.length > 1) {
      console.log("Weird Error: more than one user found?");
    }
    if (items.length === 0) {
      return res.sendStatus(401);
    }
    let userObj = items[0];
    if (!userObj) {
      return res.sendStatus(401);
    }
    let salt = userObj.salt;
    let pepper = md5(mySecretMessage);
    let hashedPassword = md5(salt + password + pepper);
    if (hashedPassword !== userObj.hash) {
      return res.sendStatus(401);
    }
    let sessionId = generateCode(userObj);
    res.cookie(cookieKey, sessionId, {maxAge: 3600*1000, httpOnly: true});
    loginChecks.sessionUser[sessionId] = userObj.username;
    let message = {username: username, result: "success"};
    return res.send(message);
  });
}

const logout = (req, res) => {
  if (!req.username) {
    return res.sendStatus(401);
  } else {
    var sid = req.cookies[cookieKey];
    delete loginChecks.sessionUser[sid];
    res.clearCookie(cookieKey);
    return res.sendStatus(200);
  }
}

const register = (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  let email = req.body.email;
  let dob = req.body.dob;
  let zipcode = req.body.zipcode;
  if (!username || !password || !email || !dob || !zipcode) {
    return res.sendStatus(400);
  }
  addUser(username, password, email, zipcode, dob);
  return res.send({result: 'success', username: username});
}

const password = (req, res) => {
  return res.send({username: req.username, 'status': 'Password will not change.'});
}

module.exports = (app) => {
  app.use(session({ secret: '7c42f8ae3d1b157a7d43310cbbddafc8' }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.post('/login', loginChecks.isLoggedInOptional, login);
  app.put('/logout', loginChecks.isLoggedIn, logout);
  app.post('/register', loginChecks.isLoggedInOptional, register);
  app.put('/password', loginChecks.isLoggedIn, password);
};
