const loginChecks = require('./loginChecks.js');
var models = require('./model.js');

function getProfile(username, callback) {
  models.Profile.find({ username: username }).exec(function(err, items) {
    callback(items);
  });
}

const getFollowing = (req, res) => {
  // if no username in the url, use logged in user.
  const user = req.params.user ? req.params.user : req.username;
  getProfile(user, function(items) {
    if (items.length > 1) {
      console.log("Weird Error: more than one profile found?");
    }
    if (items.length === 0) {
      return res.sendStatus(401);
    }
    const profileObj = items[0];
    return res.send({ username: user, following: profileObj.following });
  });
}

const addFollowing = (req, res) => {
  getProfile(req.username, function(items) {
    if (items.length > 1) {
      console.log("Weird Error: more than one profile found?");
    }
    if (items.length === 0) {
      return res.sendStatus(401);
    }
    const profileObj = items[0];
    profileObj.following.push(req.params.user);
    profileObj.save(function(error) {
      if (error) {
        console.log("error adding follower");
      }
      return res.send({ username: req.username, following: profileObj.following });
    });
  });
}

const deleteFollowing = (req, res) => {
  getProfile(req.username, function(items) {
    if (items.length > 1) {
      console.log("Weird Error: more than one profile found?");
    }
    if (items.length === 0) {
      return res.sendStatus(401);
    }
    const profileObj = items[0];
    profileObj.following = profileObj.following.filter((follow) => {
      return follow !== req.params.user;
    });
    profileObj.save(function(error) {
      if (error) {
        console.log("error deleting follower.");
      }
      return res.send({ username: req.username, following: profileObj.following });
    });
  });
}

module.exports = (app) => {
  app.get('/following/:user?', loginChecks.isLoggedIn, getFollowing);
  app.put('/following/:user', loginChecks.isLoggedIn, addFollowing);
  app.delete('/following/:user', loginChecks.isLoggedIn, deleteFollowing);
};
