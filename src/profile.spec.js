/*
 * Test suite for articles.js
 */
const expect = require('chai').expect;
const fetch = require('isomorphic-fetch');

const url = path => `http://localhost:3000${path}`;

describe('Validate Headline functionality', () => {

	// PUT headline updates the headline message,
  // i.e., GET, PUT to change the value, GET to verify it was changed
  it('should correctly update loggedInUser headline', (done) => {
    fetch(url('/login'), {
			method: 'post',
      credentials: 'include',
			body: {
				// use my existing test user
				username: 'test0',
				password: 'n0t3st',
				headers: new Headers({
          'content-type': 'application/json; charset=utf-8',
          'Accept': 'application/json'
				})
			}
		})
    .then(res => {
      fetch(url("/headline"), {
         method: 'put',
         credentials: 'include',
         body: {
           headline: 'test headline'
         }
       })
			.then(res => {
				expect(res.status).to.eql(200);
				return res.text();
			})
			.then(text => {
				let jsonText = JSON.parse(text);
				expect(jsonText.headline).to.be('test headline');
			})
    })
    .then(res => {
      fetch(url("/headlines"), { credentials: 'include' })
      .then(res => {
        expect(res.status).to.eql(200);
				return res.text();
      })
      .then(text => {
        let jsonText = JSON.parse(text);
				expect(jsonText.headlines[0].headline).to.be('test headline');
      })
    })
    .then(done)
		.catch(done)
 	}, 2000)

});
