const express = require('express');
const bodyParser = require('body-parser');
const loginChecks = require('./loginChecks.js');
var models = require('./model.js');

let newArticleId = Math.floor(Math.random()*100000000) + 1;

function getArticle(id, callback) {
  models.Article.find({ id: id }).exec(function(err, items) {
    callback(items);
  });
}

function getProfile(username, callback) {
  models.Profile.find({ username: username }).exec(function(err, items) {
    callback(items);
  });
}

const addArticle = (req, res) => {
  const newArticleDate = new Date().getTime();
  const newArticle = {
    "id": newArticleId++,
    "text": req.body.text,
    "date": newArticleDate,
    "img": null,
    "comments": [],
    "author": req.username
  };
  models.Article.create(newArticle, function(err, user) {
    if (err) {
      console.log("Error: error creating new article");
    }
  })
  res.send({ articles: [ newArticle ] });
};

const updateArticle = (req, res) => {
  const id = req.params.id;  //  required
  const commentId = req.body.commentId;
  if (!commentId) {
    models.Article.findOneAndUpdate(
      // you can only update your own articles
      { id: id, author: req.username },  // query
      { text: req.body.text},  // newData
      { upsert: false },  // do not create if not found
      function(err, items) {
        if (err) {
          return res.send(500, { error: err });
        }
        items.text = req.body.text;
        return res.send({ articles: items });
      });
  } else if (commentId === -1) { // add comment
    getArticle(id, function(items) {
      if (items.length > 1) {
        console.log("Weird Error: more than one article found?");
      }
      if (items.length === 0) {
        return res.sendStatus(401);
      }
      let articleObj = items[0];
      let newComment = {
        commentId: articleObj.comments.length + 1,
        author: req.username,
        date: new Date().getTime(),
        text: req.body.text
      };
      articleObj.comments.push(newComment);
      articleObj.save(function(error) {
        if (error) {
          console.log("error saving article with new comment.");
          return res.send(500, { error: err });
        }
        return res.send({ articles: articleObj });
      });
    });
  } else {
    getArticle(id, function(items) {
      if (items.length > 1) {
        console.log("Weird Error: more than one article found?");
      }
      if (items.length === 0) {
        return res.setStatus(401);
      }
      let articleObj = items[0];
      let comments = articleObj.comments;
      let newComments = [];
      comments.forEach((comment) => {
        if (comment.commentId === commentId) {
          comment.text = req.body.text;
        }
        newComments.push(comment);
      });
      articleObj.comments = newComments;
      articleObj.save(function(error) {
        if (error) {
          console.log("error saving article with updated comment.");
          return res.send(500, { error: err });
        }
        return res.send({ articles: articleObj });
      });
    });
  }
};

function getArticlesByAuthors(usersToQuery, limit, callback) {
  models.Article.find({
    author: {
      $in: usersToQuery
    }
  })
  .sort({date: -1})
  .limit(10)
  .exec(function (error, items) {
    if (error) {
      console.log("error: failed to query articles by authors.");
    }
    callback(items);
  });
}

function getFeedArticles(username, includeFollowing, callback) {
  getProfile(username, function(items) {
    if (items.length > 1) {
      console.log("Weird Error: more than one profile found?");
      callback([]);
    }
    if (items.length === 0) {
      console.log("error: no profile found.");
      callback([]);
    }
    const profileObj = items[0];
    let usersToQuery = [];
    if (includeFollowing) {
      usersToQuery = usersToQuery.concat(profileObj.following);
    }
    usersToQuery.push(username);
    getArticlesByAuthors(usersToQuery, 10, function(articles) {
      callback(articles);
    });
  });
}

const getArticles = (req, res) => {
  const id = req.params.id;
  if (!id) { // get the logged in user's feed
    getFeedArticles(req.username, true, function(items) {
      return res.send({ articles: items });
    });
  } else { // get all articles by the specified user, or the specified article
    // here id would be an article id
    getArticle(id, function(items) {
      if (items.length > 1) {
        console.log("Weird Error: more than one article found?");
      }
      if (items.length === 1) {
        return res.send({ articles: items[0] });
      }
      if (items.length === 0) {
        // here id would be a username
        getFeedarticles(id, false, function(items) {
          return res.send({ articles: items });
        });
      }
    });
  }
};

module.exports = (app) => {
  app.post('/article', loginChecks.isLoggedIn, addArticle);
  app.get('/articles/:id*?', loginChecks.isLoggedIn, getArticles);
  app.put('/articles/:id', loginChecks.isLoggedIn, updateArticle);
};
