/*
 * Middleware functions to get the loggedInUser shared by all endpoints.
 */

var cookieKey = 'sid';
var sessionUser = {};

function isLoggedIn(req, res, next) {
    var sid = req.cookies[cookieKey];
    if (!sid) {
      return res.sendStatus(401);
    }
    let username = sessionUser[sid];
    if (username) {
      req.username = username;
    } else {
      res.sendStatus(401);
    }
    return next();
}

function isLoggedInOptional(req, res, next) {
  var sid = req.cookies[cookieKey];
  if (!sid) {
    return next();
  }
  let username = sessionUser[sid];
  if (username) {
    req.username = username;
  }
  return next();
}

module.exports = {
  isLoggedIn: isLoggedIn,
  isLoggedInOptional: isLoggedInOptional,
  sessionUser: sessionUser
}
