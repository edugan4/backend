if (process.env.NODE_ENV !== "production") {
    require('dot-env')
}
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const articles = require('./src/articles.js');
const auth = require('./src/auth.js');
const profile = require('./src/profile.js');
const following = require('./src/following.js');
const cloudinary = require('./uploadCloudinary.js');

const hello = (req, res) => res.send({ hello: 'world' });

const app = express();
app.use(function(req, res, next) {
  if (req.method === 'OPTIONS') {
    res.status(200)
  }
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());
app.use(cookieParser());
app.get('/', hello); // TODO: change the root endpoint?
articles(app);
auth(app);
profile(app);
following(app);

// Get the port from the environment, i.e., Heroku sets it
const port = process.env.PORT || 3000
const server = app.listen(port, () => {
     const addr = server.address()
     console.log(`Server listening at http://${addr.address}:${addr.port}`)
})
