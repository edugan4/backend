SLIP DAYS: 3


design decision for article/userIDs:
  user ids are simply their usernames
  article id's are randomly generated unique numbers

i think this is a valid approach for the scale of our project, but a project of larger scale would probably use the mongo generated IDs.
